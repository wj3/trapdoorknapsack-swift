//
//  HelperFunction.swift
//  TrapdoorKnapsack
//
//  Created by Wang Jing on 4/10/18.
//  Copyright © 2018 UOW. All rights reserved.
//

//import Foundation
import BigInt

public func generateSuperIncresingSequence(n: Int) -> Array<Int> {
    var sequence = Array<Int>()
    let first = Int.random(in: 1..<10)
    sequence.append(first)
    var sum = first
    var counter = 1
    while counter < n {
        let randomInt = Int.random(in: 1..<min(sum,50))
        let num = sum+randomInt
        sum+=num
        sequence.append(num)
        counter+=1
    }
    return sequence
}

public func generatePrime(length: Int) -> BigUInt {
    while true {
        var random = BigUInt.randomInteger(withExactWidth: length)
        random |= BigUInt(1)
        if random.isPrime() {
            return random
        }
    }
}

public func coPrime(n: Int, m: Int) -> Bool {
    let gcd = BigUInt(n).greatestCommonDivisor(with: BigUInt(m))
    if (gcd == BigUInt(1)){
        return true;
    }
    return false;
}

/*private static String binaryString(int n){
    return String.format("%8s", Integer.toBinaryString(n)).replace(' ', '0');
}*/

public func binaryString(n: Int) -> String {
    let str = String(n, radix: 2)
    return pad(string: str, toSize: 8)
}

public func pad(string : String, toSize: Int) -> String {
    var padded = string
    for _ in 0..<(toSize - string.count) {
        padded = "0" + padded
    }
    return padded
}

public func binarytoString(s: String) -> String {
    var result = ""
    var temp = ""
    var i = 0
    for c in s {
        if(i%8==0 && i != 0){
            let dec = UInt8(strtoul(temp, nil, 2))
            result+=String(UnicodeScalar(dec))
            temp = ""
            temp.append(c)
        }else{
            temp.append(c);
        }
        if(i==s.count-1){
            let dec = UInt8(strtoul(temp, nil, 2))
            result+=String(UnicodeScalar(dec))
        }
        i+=1
    }
    return result;
}

public func gcd(p: Int, q: Int) -> BigUInt {
    return BigUInt(p).greatestCommonDivisor(with: BigUInt(q))
}

public func inverse(w: BigUInt, modulus: BigUInt) -> Int {
    let inv = w.inverse(modulus)
    if inv != nil {
        return Int(inv!)
    }
    return 1
}

public func calcMod(y: Int, inv: Int, modululs: Int) -> BigUInt {
    return BigUInt(y).power(BigUInt(inv), modulus: BigUInt(modululs))
}

public func decryptWithKeys(cipher: Array<Int>, secretKey: Array<Int>, modulus: Int, inv: Int) -> String {
    var binary = ""
    for n in cipher {
        let y = n * inv % modulus
        //print(y)
        var reverse = ""
        var val = y
        for key in secretKey.reversed() {
            if(val >= key){
                reverse+="1"
                val=val-key
            }else{
                reverse+="0"
            }
        }
        let original = String(reverse.reversed())
        binary.append(original)
    }
    let result = binarytoString(s: binary)
    
    return result
}

extension StringProtocol {
    var ascii: [UInt32] {
        return unicodeScalars.compactMap {$0.isASCII ? $0.value : nil}
    }
}
