//
//  ViewController.swift
//  TrapdoorKnapsack
//
//  Created by Wang Jing on 4/10/18.
//  Copyright © 2018 UOW. All rights reserved.
//

import UIKit
import BigInt

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtKeySize: UITextField!
    @IBOutlet weak var btnGenerateKey: UIButton!
    @IBOutlet weak var txtResult: UITextView!
    @IBOutlet weak var btnEncrypt: UIButton!
    @IBOutlet weak var btnDecrypt: UIButton!
    @IBOutlet weak var txtCipher: UITextField!
    
    var keySize: Int = 0
    var p: BigUInt = 0
    var w: BigUInt = 0
    var secretKeys = Array<Int>()
    var publicKeys = Array<Int>()
    var inv: Int = 1
    var encrypted = Array<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //test()
        setupUI()
        
    }
    
    func setupUI() {
        txtResult.text = "Welcome to Trapdoor Knapsack Simulator!"
        txtResult.isEditable = false
        txtKeySize.delegate = self
        
        btnGenerateKey.layer.borderColor = UIColor.black.cgColor
        btnGenerateKey.layer.borderWidth = 1.0
        btnGenerateKey.layer.cornerRadius = 5.0
        
        btnEncrypt.layer.borderColor = UIColor.black.cgColor
        btnEncrypt.layer.borderWidth = 1.0
        btnEncrypt.layer.cornerRadius = 5.0
        
        btnDecrypt.layer.borderColor = UIColor.black.cgColor
        btnDecrypt.layer.borderWidth = 1.0
        btnDecrypt.layer.cornerRadius = 5.0
    }

    func test() {
        let binary = 84
        print("2 ^ 5 % 10 = \(calcMod(y: 2, inv: 5, modululs: 10))")
        print("gcd(150, 35) = \(gcd(p:150, q:35))")
        print("Binary String \(binary): \(binaryString(n: binary))")
        let seq = generateSuperIncresingSequence(n: 8)
        for num in seq {
            print("\(num)")
        }
    }
    
    func encrypt(plaintext: String) -> String {
        encrypted = []
        let str = plaintext
        let byteArr = str.ascii
        var binaryStr = ""
        for n in byteArr {
            let s = binaryString(n: Int(n))
            binaryStr+=s
            //print(s)
        }
        var temp = 0
        print("Binary String: \(binaryStr)")
        var counter = 0
        for c in binaryStr {
            let value = Int(String(c)) ?? 0
            let multi = publicKeys[counter%keySize]
            if(counter%keySize == 0 && counter != 0) {
                encrypted.append(temp)
                temp = value*multi
            }else{
                temp += value*multi
            }
            if(counter==binaryStr.count-1){
                encrypted.append(temp)
            }
            counter+=1
        }
        print("Ciphertext: \(encrypted)")
        return String("\(encrypted)")
    }
    
    @IBAction func generateKeys(_ sender: Any) {
        txtKeySize.resignFirstResponder()
        txtCipher.text = ""
        let size = txtKeySize.text ?? ""
        if (size != ""){
            print("Key Size: \(size)")
        }else{
            return
        }
        keySize = Int(size) ?? 0
        secretKeys = generateSuperIncresingSequence(n: keySize)
        //let secretKeys = [2,5,9,21,45,103,215,450]
        let sum = secretKeys.reduce(0, +)
        var length = keySize
        while (pow(2, length) < Decimal(sum)) {
            length+=1
        }
        p = generatePrime(length: length)
        //let p = BigUInt(851)
        w = generatePrime(length: keySize)
        //let w = BigUInt(199)
        if (!coPrime(n: Int(p), m: Int(w))){
            print("p and w are not co-prime")
        }
        inv = inverse(w: w, modulus: p)
        //print("Inverse: \(inv)")
        publicKeys = []
        for n in secretKeys {
            let b = w.multiplied(by: BigUInt(n)) % p
            publicKeys.append(Int(b))
        }
        print(secretKeys)
        let output = "Secret Keys: \(secretKeys)\n" +
        "(Modulus, Multiplier): (\(p), \(w))\n" +
        "Public Keys: \(publicKeys)\n"
        txtResult.text = output
        
    }
    
    @IBAction func encrypt(_ sender: Any) {
        let plaintext = txtCipher.text ?? ""
        if(secretKeys.count == 0) {
            return
        }
        let ciphertext = encrypt(plaintext: plaintext)
        txtResult.text.append("Encrypting...\n" +
            "Ciphertext: " + ciphertext + "\n")
        txtCipher.text = ciphertext
        if(txtResult.contentSize.height > txtResult.frame.height){
            let bottom = NSMakeRange(txtResult.text.count - 1, 1)
            txtResult.scrollRangeToVisible(bottom)
        }
    }
    
    @IBAction func decrypt(_ sender: Any) {
        if(secretKeys.count == 0) {
            return
        }
        let plaintext = decryptWithKeys(cipher: encrypted, secretKey: secretKeys, modulus: Int(p), inv: inv)
        txtResult.text.append("Decrypting...\n" +
            "Plaintext: " + plaintext + "\n")
        txtCipher.text = plaintext
        if(txtResult.contentSize.height > txtResult.frame.height){
            let bottom = NSMakeRange(txtResult.text.count - 1, 1)
            txtResult.scrollRangeToVisible(bottom)
        }
    }
    
    
}

